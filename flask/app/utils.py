import json


def get_cfg_json():
    with open("pgsqlcfg.json", "r") as file :
        return json.load(file)

def build_database_uri(data):
    return "postgresql://{}:{}@{}:{}/{}".format(data["user"],data["pw"],data["host"],data["port"],data["db"])