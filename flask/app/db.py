import os
from utils import build_database_uri, get_cfg_json
from flask_sqlalchemy import SQLAlchemy

PGSQL_CONNECTION_STRING = ""
CFG = get_cfg_json()


if 'FLASK_ENV' in os.environ:
    env = os.environ['FLASK_ENV']
    if env == "development":
        PGSQL_CONNECTION_STRING = build_database_uri(CFG['postgres_dev'])
    elif env == "docker" or env == "prod":
        PGSQL_CONNECTION_STRING = build_database_uri(CFG['postgres_docker'])
else:
    PGSQL_CONNECTION_STRING = build_database_uri(CFG['postgres_dev'])

db = SQLAlchemy()
