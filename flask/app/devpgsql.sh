echo "enabling database"

# pull image if not on your machine yet
docker pull postgres 
# start pg and mount to local dev volume
docker run --rm --name pg-dev-docker -e POSTGRES_PASSWORD=example -e POSTGRES_DB=quotes  -d -p 5432:5432 -v $PWD/devpg:/var/lib/postgresql/data postgres 