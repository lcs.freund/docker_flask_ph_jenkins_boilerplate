from db import db
# pylint: disable=no-member
class Quote(db.Model):
    """ Just some Quote """
    id = db.Column(db.Integer, primary_key=True)
    qoute = db.Column(db.String(255), nullable=False)
    by_who = db.Column(db.String(255), nullable=True)


class User(db.Model):
    """ User Example """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=True)

