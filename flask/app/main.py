from db import db, PGSQL_CONNECTION_STRING
import os
from flask import Flask, render_template
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
import models
app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = PGSQL_CONNECTION_STRING
print(PGSQL_CONNECTION_STRING)
db.init_app(app)

@app.route('/')
def index():
    return render_template('base.html')
@app.route('/hello')
def hello_world():
    return "Hello World"





if __name__ == '__main__':

    env = os.environ['FLASK_ENV']
    print("Starting Server for env: {}".format(env))

    # start flask admin
    app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'  # why tho?
    app.secret_key = "super_secret_but_not_secure"
    admin = Admin(app, name='quotes', template_mode='bootstrap3')

    # Add administrative views here

    admin.add_view(ModelView(models.User, db.session))
    admin.add_view(ModelView(models.Quote, db.session))

    if env == "development":
        app.run(host='127.0.0.1', port=5000, debug=True)
    elif env == "docker":
        app.run(host='0.0.0.0', port=8000, debug=True)
    elif env == "prod":
        app.run(host='0.0.0.0', port=8000)
