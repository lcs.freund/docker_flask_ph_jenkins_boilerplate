#!/bin/bash
export FLASK_ENV=development

if [ ! -d "venv" ]; then
    echo "you need to create a virtual environment first"
    echo "you can do so by running localdevenv"
    exit 1
else
    source venv/bin/activate
    python main.py
fi  

