echo "Initializing DB"

docker exec flask python manage.py db init
docker exec flask python manage.py db migrate
docker exec flask python manage.py db upgrade
