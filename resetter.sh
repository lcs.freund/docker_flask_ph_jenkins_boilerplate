echo "Resetting Postgres Volume"
echo "this command must run as super user, if it fails you need to run it as sudo"
rm -rf pg/postgres-data
exit_status=$?
echo $exit_status
if [[ exit_status -eq 0 ]]; then
        echo "Removal of postgres volume successful"
else
        echo "Please rerun with sudo."
        exit 1
fi
echo "Rebuild and run all? y|n "
read rebuild

if [[ "$rebuild" == n ]]; then
        echo "Database wont be able to connect since volume was deleted"
        echo "Rerun and select y to fix"
        exit 0
fi
echo "the following steps will be ran as user $SUDO_USER"
sudo -u $SUDO_USER docker container stop $(docker ps -aq)
sudo -u $SUDO_USER docker container prune

sudo -u $SUDO_USER docker-compose build
sudo -u $SUDO_USER docker-compose up -d

echo "Would you like to run migrator now? y|n "

read migrate
if [[ "$migrate" == "y" ]]; then
        ./migrator.sh
        echo "migrator ran"
fi
echo "And we are done"


